package com.pavelchak.annotation;

import java.lang.annotation.*;

@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface MyField {

  String name() default "Andrii";

  int age() default 40;
}
