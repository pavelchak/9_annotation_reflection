package com.pavelchak.model;

import com.pavelchak.annotation.MyField;

public class MyClassA {

  @MyField(name = "int:integerValue")
  private int integerValue;
  @MyField
  private String name;
  private double doubleValue;

  private void getInfo(){
    System.out.println("HELLO from getInfo()!");
  }

  public int getIntegerValue() {
    return integerValue;
  }

  public void setIntegerValue(int integerValue) {
    this.integerValue = integerValue;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getDoubleValue() {
    return doubleValue;
  }

  public void setDoubleValue(double doubleValue) {
    this.doubleValue = doubleValue;
  }

  @Override
  public String toString() {
    return "MyClassA{" +
        "integerValue=" + integerValue +
        ", name='" + name + '\'' +
        ", doubleValue=" + doubleValue +
        '}';
  }
}
