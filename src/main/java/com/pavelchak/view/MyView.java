package com.pavelchak.view;

import com.pavelchak.annotation.MyField;
import com.pavelchak.model.MyClassA;
import com.pavelchak.model.MyGenericClass;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class MyView {

  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    menu.put("1", "  1 - Test field with annotation");
    menu.put("2", "  2 - Test unknown object");
    menu.put("3", "  3 - Test Generic");

    menu.put("Q", "  Q - exit");

    methodsMenu.put("1", this::test1);
    methodsMenu.put("2", this::test2);
    methodsMenu.put("3", this::test3);
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void test1() {
    System.out.println("1 - Test field with annotation");
    Class clazz = MyClassA.class;
    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      // if field is annotated with @MyField
      if (field.isAnnotationPresent(MyField.class)) {
        MyField myField = (MyField) field.getAnnotation(MyField.class);
        String name = myField.name();
        int age = myField.age();
        System.out.println("field: " + field.getName());
        System.out.println("  name in @MyField: " + name);
      }
    }
  }

  private void test2() {
    System.out.println("2 - Test unknown object");
    MyClassA myClassA = new MyClassA();
    reflUnknownObject(myClassA);
  }

  void reflUnknownObject(Object object) {
    try {
      Class clazz = object.getClass();
      System.out.println("The name of class is " + clazz.getSimpleName());

      // Getting the constructor of the class
      Constructor constructor = clazz.getConstructor();
      System.out.println("The name of constructor is " + constructor.getName());
      //------------------------------------------------------------------------
      System.out.println("The declared methods of class are : ");
      Method[] methods = clazz.getDeclaredMethods();
      // Printing method names
      for (Method method : methods) {
        System.out.println("  " + method.getName());
      }
      System.out.println();
      //------------------------------------------------------------------------
      System.out.println("The declared fields of class are : ");
      Field[] fields = clazz.getDeclaredFields();
      // Printing field names
      for (Field field : fields) {
        System.out.println("  " + field.getName());
      }
      System.out.println();
      //------------------------------------------------------------------------
      fields[0].setAccessible(true);
      if (fields[0].getType() == int.class) {
        fields[0].setInt(object, 47);
      }
      System.out.println(object);
      //------------------------------------------------------------------------
      Method methodcall = clazz.getDeclaredMethod("getInfo");
      methodcall.setAccessible(true);
      // invokes the method at runtime
      methodcall.invoke(object);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void test3() {
    System.out.println("3 - Test Generic");
    MyGenericClass<MyClassA> obj = new MyGenericClass<>(MyClassA.class);
  }



  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
