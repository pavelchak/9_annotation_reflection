package com.pavelchak;

import com.pavelchak.view.MyView;

public class Main {

    public static void main(String[] args) {
        MyView myView = new MyView();
        myView.show();
    }
}
